package com.tc.itfarm.service;

import com.tc.itfarm.model.Comment;
import com.tc.itfarm.model.ext.CommentVO;

import java.util.List;

/**
 * Created by wangdongdong on 2016/9/20.
 */
public interface CommentService extends BaseService<Comment> {

    List<CommentVO> selectByArticle(Integer articleId);

    /**
     * 查询文章的评论数
     * @param articleId
     * @return
     */
    Integer countByArticle(Integer articleId);

    /**
     * 查询最近十条评论
     * @return
     */
    List<Comment> selectRecent10();
}
