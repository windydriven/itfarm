<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
<div id="footer">

    <div class="container clearfix">

        <a href="index.html"><img src="img/footer-logo.png" alt="footer-logo" class="footer-logo" /></a>

        <div class="one-third">

            <h4>About Our Community</h4>

            <p>Lorem Ipsum is simply dummy text of the <a href="#">printing and typesetting</a> industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic...</p>

            <p>Lorem Ipsum is simply dummy <a href="#">text of the printing</a> and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an...</p>

            <strong><a href="#">Find more...</a></strong>

        </div><!-- end .one-third -->

        <div class="one-fourth">

            <h4>Categories</h4>

            <ul id="categories">
                <li><a href="#">Photography</a></li>
                <li><a href="#">Private Portfolios</a></li>
                <li><a href="#">My Travels</a></li>
                <li><a href="#">Family Travel Guide</a></li>
                <li><a href="#">Fly&amp;Drive Guides</a></li>
                <li><a href="#">Tropical Destinations</a></li>
                <li><a href="#">Mountains Desctinations</a></li>
            </ul>

            <strong class="align-center"><a href="#">Find more...</a></strong>

        </div><!-- end .one-fourth -->

        <div class="two-fifth last">

            <h4><span>Latest</span> Tweets</h4>

            <ul id="latest-tweets">

                <li>
                    <a href="#" class="user">@medioworks</a>
                    <p class="tweet">Lorem Ipsum is simply dummy text of <a href="#">the printing and typesetting</a> industry. Lorem Ipsum has been the industry's standard dummy text...</p>
                </li>

                <li>
                    <a href="#" class="user">@medioworks</a>
                    <p class="tweet">Lorem Ipsum is simply <a href="#">dummy text</a> of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text...</p>
                </li>

                <li>
                    <a href="#" class="user">@medioworks</a>
                    <p class="tweet">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text...</p>
                </li>

            </ul><!-- end #latest-tweets -->

            <strong class="align-right"><a href="#">Join Conversation</a></strong>

        </div><!-- end .one-misc -->

    </div><!-- end .container -->

</div><!-- end #footer -->

<div id="footer-bottom">

    <div class="container">

        <p class="align-left">ITFARM - Copyright 2010, All Rights Reserved.</p>

        <ul class="align-right">
            <li><a href="#">Login</a><span class="separator">|</span></li>
            <li><a href="#">SignUp</a><span class="separator">|</span></li>
            <li><a href="#">Support</a><span class="separator">|</span></li>
            <li><a href="#">Contact Us</a></li>
        </ul>

    </div><!-- end .container -->

</div><!-- end #footer-bottom -->
</body>
</html>