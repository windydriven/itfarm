package com.tc.itfarm.api.util;

import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by Administrator on 2016/9/4.
 */
public final class CollectionUtil {

    private static final Logger logger = LoggerFactory.getLogger(CollectionUtil.class);

    private CollectionUtil(){};

    /**
     * 数组转集合
     * @param array
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> arrayToList(T [] array, Class<T> clazz) {
        if (array.length < 1) {
            return Lists.newArrayList();
        }
        List<T> list = Lists.newArrayList();
        for (T i : array) {
            list.add(i);
        }
        return list;
    }
}
